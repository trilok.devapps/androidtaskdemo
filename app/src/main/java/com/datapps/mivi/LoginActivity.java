package com.datapps.mivi;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends MIVIActivity {

    private Context mContext = this;
    public static final String TAG = "LoginActivity";
    private String mMSN = null;
    private EditText etMSN;
    private TextView tvLogin;
    private TextView tvSkip;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Hide title bar
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        mMSN = readJsonFile().getIncluded().get(0).getAttributes().getMsn();

        etMSN = findViewById(R.id.et_msn);
        tvLogin = findViewById(R.id.tv_login);
        tvSkip = findViewById(R.id.tv_skip);

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMSN != null) {
                    String msn = etMSN.getText().toString().trim();
                    if(msn.equals(mMSN)){
                        loadSplashScreen();
                    }else {
                        showAlert();
                    }
                }
            }
        });

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadSplashScreen();
            }
        });
    }

    private void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setMessage(getResources().getString(R.string.msn_alert_msg));
        builder.setCancelable(false);
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }



    private void loadSplashScreen(){
        Intent intent = new Intent(mContext, SplashScreenActivity.class);
        startActivity(intent);
    }
}
